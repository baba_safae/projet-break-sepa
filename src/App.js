import './App.css';
import Creer from './components/Creer';
import ListeAttente from './components/ListeAttente';

function App() {
  return (
    <div className="App">
      <Creer />
      <ListeAttente />
    </div>
  );
}

export default App;
