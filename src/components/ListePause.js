import React from 'react'
import {useEffect, useRef, useState} from 'react'


const ListePause = ({ props }) => {
    const [timer, setTimer] = useState([...props]);
    const countRef = useRef(0);

    useEffect(() => {
        countRef.current = setInterval(() => {
            let listTimer = [...props];
            for(let utilisateur of listTimer){
                utilisateur.pause = utilisateur.pause-1;
            }
            setTimer(listTimer);
        }, 1000);
        return () => clearInterval(countRef.current);
      }, [props]);

      const terminer = (id) =>{
          setTimer(timer.filter(e=>e.id!=id));
      }

    const formatTime = (timer) => {
        const getSeconds = `0${timer % 60}`.slice(-2);
        const minutes = `${Math.floor(timer / 60)}`;
        const getMinutes = `0${minutes % 60}`.slice(-2);
        return getMinutes + `:` + getSeconds;
    };

    return (
        <div>
            {
                timer.map((Utilisateur) => {
                    return (
                        <p>
                            <strong>{Utilisateur.name} </strong> en pause depuis <strong>{formatTime(Utilisateur.pause)} </strong>
                            <button onClick = {() => terminer(Utilisateur.id)}>Terminer</button>
                        </p>
                    )
                })
            }
        </div>
    )
}

export default ListePause
