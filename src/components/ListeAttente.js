import React from 'react'
import { useEffect, useState } from 'react'
import firebase from '../Util/firebase'
import ListePause from './ListePause';

const ListeAttente = () => {

    //// Partie Affichage 
    const [Liste, setListe] = useState([]);
    const [test, setTest] = useState([]);

    useEffect(() => {
        const db = firebase.database().ref("UtilisateursDB");
        db.on("value", (snapshot) => {
            let Utilisateurs = snapshot.val();
            let A = [];
            for (let id in Utilisateurs) {
                A.push({ id, ...Utilisateurs[id] });
            }
            setListe(A);
        });
    }, []);
    //fuyfuyfy
    const formatTime = (timer) => {
        const getSeconds = `0${timer % 60}`.slice(-2);
        const minutes = `${Math.floor(timer / 60)}`;
        const getMinutes = `0${minutes % 60}`.slice(-2);
        return getMinutes + `:` + getSeconds;
    };

    //// Partie chrono

    function Demmarer(Utilisateur) {
        var T = Date.now();
        //console.log(test);
        setTest([Utilisateur, ...test])
        // setComps([...Comps, <UserPause User={Utilisateur} />]);
    };


    return (
        <>
            <div className="flex" >
            <li>
                <div>
                        <ListePause props={test} />

                </div>
            </li>
            <li>
                {
                    Liste.map((Utilisateur) => {
                        let rest= Utilisateur.pause;
                        return (
                            <>
                                <div>
                                    <strong>ID :</strong> {Utilisateur.name}
                                    <strong> Temps Restans:</strong> { formatTime (rest)}
                                    <button style={{ background: "green" }} onClick={() => Demmarer(Utilisateur)}>Commencer</button>
                                    <button style={{ background: "orange" }}>Annuler</button>
                                    <button style={{ background: "red" }}>Supprimer</button>
                                </div>
                            </>
                        )
                    })}
            </li>
        </div>
        </>
    )
}

export default ListeAttente
