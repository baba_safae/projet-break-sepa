import firebase from 'firebase'

  // Your web app's Firebase configuration
  // For Firebase JS SDK v7.20.0 and later, measurementId is optional
  const firebaseConfig = {
    apiKey: "AIzaSyDJA4jzQAcys-68nbeVfP3tCqWtaYR8rmY",
    authDomain: "mon-projet-de202.firebaseapp.com",
    projectId: "mon-projet-de202",
    storageBucket: "mon-projet-de202.appspot.com",
    messagingSenderId: "952590111446",
    appId: "1:952590111446:web:cd0fa801d42d6a8b4d6f77"
  };
  // Initialize Firebase
  firebase.initializeApp(firebaseConfig);
  firebase.analytics();

export default firebase;